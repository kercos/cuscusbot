import key
from utility import escape_markdown

import json
import logging

# https://firebase.google.com/docs/firestore/manage-data/add-data
from google.cloud import firestore

from dataclasses import dataclass, field
from typing import List, Dict, Any

# https://gitlab.com/futureprojects/firestore-model/blob/master/examples/main.py
import firestore_model
from firestore_model import Model

db = firestore.Client()
firestore_model.db = db


@dataclass
class User(Model):
    application: str
    chat_id: str
    name: str
    username: str
    language: str
    bot: bool = False
    state: str = None
    previous_state: Dict = field(default_factory=dict)
    keyboard: Dict = field(default_factory=dict)
    notifications: bool = True
    current_game_id: str = None
    variables: Dict = field(default_factory=dict)

    @staticmethod
    def make_id(application, chat_id):
        return '{}_{}'.format(application, chat_id)

    def __eq__(self, other):
        return type(self) == type(other) and self.id == other.id

    @staticmethod
    def create_user(application, chat_id, name, username, language, bot=False):
        user = User.make(
            application = application,
            chat_id = str(chat_id),
            name = name,
            username = username,
            language = language if language in ['en','it'] else 'it',
            bot = bot
        )
        user.id = User.make_id(application, chat_id)
        user.save()
        return user

    @staticmethod
    def get_user(application, chat_id):
        id_str = User.make_id(application, chat_id)
        return User.get(id_str)

    def update_user(self, name, username):
        self.name = name
        self.username = username
        self.save()

    def get_name(self):
        return escape_markdown(self.name)

    def get_name_at_username(self, escape_markdown=False):
        if self.username:
            result = "{} @{}".format(self.name, self.username)
        else:
            result = self.name 
        if escape_markdown:
            return escape_markdown(result)
        return result

    def set_state(self, state, set_previous_state=True, save=True):
        if set_previous_state:
            self.previous_state[state] = self.state
        self.state = state
        if save: self.save()

    def get_previous_state(self):
        return self.previous_state[self.state]

    def switch_language(self, save=True):
        self.language = 'it' if self.language == 'en' else 'en'
        if save: self.save()

    def switch_notifications(self):
        self.notifications = not self.notifications
        self.save()

    def set_keyboard(self, value, save=True):
        self.keyboard = {str(i):v for i,v in enumerate(value)}
        if save: self.save()

    def set_empy_keyboard(self, save=True):
        self.keyboard = {}
        if save: self.save()

    def get_keyboard(self):
        if self.keyboard:
            return [self.keyboard[str(i)] for i in range(len(self.keyboard))] 
        return []

    def reset_variables(self, save=True):
        self.variables = {}
        if save: self.save()

    def set_var(self, var_name, var_value, save=False):
        self.variables[var_name] = var_value
        if save: self.save()

    def get_var(self, var_name):
        return self.variables.get(var_name,None)

    def is_master(self):
        return self.chat_id == key.TELEGRAM_BOT_MASTER_ID
    
    def is_admin(self):
        return self.chat_id in key.TELEGRAM_ADMIN_IDS

    @staticmethod
    def get_user_lang_state_notification_on(lang, state):
        users_generator = User.query([
            ('notifications', '==', True), 
            ('state', '==', state)
        ]).get()
        return list(users_generator)

    @staticmethod
    def get_user_count():
        return sum(1 for _ in User.query().get())

def get_fede():
    return User.get_user('telegram', key.TELEGRAM_BOT_MASTER_ID)

if __name__ == "__main__":
    user = User.create_user('test','123','name','username','it')
    