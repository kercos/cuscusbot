import utility
from utility import escape_markdown as exmd
# ================================
# SYMBOLS
# ================================
# 🤗📝✏️
CHECK_SYMBOL = '✅'
CANCEL_SYMBOL = '❌'
BLACK_CHECK_SYMBOL = '✔️'
BULLET_SYMBOL = '∙'
RIGHT_ARROW_SYMBOL = '→'
IT_FLAG_SYMBOL = '🇮🇹'
EN_FLAG_SYMBOL = '🇬🇧'
CHEF_M = '‍👨‍🍳' 
CHEF_F = '👩‍🍳'

# ================================
# BUTTONS
# ================================

BUTTON_YES = '👌 SI'
BUTTON_NO = '🙅‍♀️ NO'
BUTTON_BACK = "🔙 INDIETRO"
BUTTON_HOME = "🏠 INIZIO"
BUTTON_INFO = "ℹ️ INFO"
BUTTON_ABORT = "❌ ANNULLA"
BUTTON_SKIP = "➡️ SALTA"
BUTTON_REMOVE = "🗑️ CANCELLA"
BUTTON_CONTACT_US = "📩 CONTATTACI"
BUTTON_ADMIN = "🔑 Admin"
BUTTON_SEARCH_DISH = "🔍🍽️ Cerca Piatto"
BUTTON_SEARCH_CHEF = "🔍👨‍🍳👩‍🍳 Cerca Chef"
BUTTON_SEARCH_BY_COUNTRY = "🌍 PAESE"
BUTTON_SEARCH_BY_LOCATION = "📍 ZONA di TRENTO"
BUTTON_SEARCH_ALL = "♨️ TUTTI"
BUTTON_CATERING = "🎉🎈 Organizza il tuo evento"
BUTTON_ENABLE_NOTIFICATIONS = "🔕 → 🔔"
BUTTON_DISABLE_NOTIFICATIONS = "🔔 → 🔕"
BUTTON_STATS = '📊 STATS'
BUTTON_REFRESH_CHEF_DATA = '📥 REFRESH CHEF INFO'
BUTTON_CHEF_M_INFO = '👨‍🍳 CONOSCI LO CHEF'
BUTTON_CHEF_F_INFO = '👩‍🍳 CONOSCI LA CHEF'
BUTTON_ORDER_DISH = "✅ Prenota piatto"

####################
# FLAGS
####################
FLAGS = {
    'Italia':	'🇮🇹',
    'Iran':	'🇮🇷',
    'Camerun':	'🇨🇲',
    'Marocco':	'🇲🇦',
    'Senegal':	'🇸🇳',
    'Colombia':	'🇨🇴',
    'Georgia':	'🇬🇪',
    'Brasile':	'🇧🇷'

}

def insert_flag(country, text=None): 
    if text==None:
        text = country
    return '{} {}'.format(FLAGS.get(country,'❓'), text)

####################
# CONVERSATIONS
####################

MSG_INFO = (
    "Sei stanco di mangiare la solita pizza o il kebab? Sei curioso di assaggiare nuovi piatti?  Allora @CusCusTrentoBot è per un buongustaio come te! 😋"
    "\n\n"
    "Puoi cercare piatti di diverse tradizioni culinarie, preparati da chef amatoriali selezionati che hanno partecipato al progetto CusCus per acquisire competenze ed attestati riconosciuti nel mondo della ristorazione, e che hanno preparato pranzi, cene e buffet in occasione di diverse manifestazioni a Trento."
    "\n\n"
    "Scopri il piatto che fa per te e contatta la/lo chef per metterti d'accordo sulla modalità di scambio!"
    "\n\n"
    "Per maggiori info sul progetto CusCus: www.cuscus.org"
    "\n\n"
    "Curiosità, dubbi, perplessità sull'utilizzo del bot? Scrivici a info@cuscus.org"
)

MSG_WELCOME = "🤗 Benvenuto/a in CusCusTrentoBot!"
MSG_HOME = "🏠 Schermata Iniziale"
MSG_HOME_TEST = "🏠 Schermata Iniziale\n🧪 Attenzione sei nel bot di test!"
MSG_HOME_CHEF_M = "🏠👨‍🍳 Schermata Iniziale Chef"
MSG_HOME_CHEF_F = "🏠👩‍🍳 Schermata Iniziale Chef"
MSG_REFRESH_INTERFACE = "🤖 Il chatbot è stato aggiornato."
MSG_NOTIFICATIONS_ON = "🔔 Hai le notifiche abilitate."
MSG_NOTIFICATIONS_OFF = "🔕 Hai le notifiche disabilitate."
MSG_WORK_IN_PROGRESS = "🏗 Sistema in manutenzione, riprova più tardi."
MSG_TO_BE_IMPLEMENTED = "🚧 To be implemented..."
MSG_FEATURE_NOT_YET_IMPLEMENTED = "🏗 Questa funzionalità non è ancora stata implementata."
MSG_CHAT_SENT = "📩 Messaggio inviato."
MSG_SELECT_DISH_SEARCH_CRITERION = "🔍 Selezionare il criterio per la ricerca di un piatto."
MSG_SELECT_COUNTRY = "🔍🌍 Scegli il paese del piatto che stai cercando."
MSG_SELECT_LOCATION = "🔍📍 Selezionare una zona di Trento dei piatti che vuoi visualizzare."
MSG_SELECT_CHEF = "🔍👨‍🍳👩‍🍳 Selezionare uno chef"
MSG_SELECT_DISH_BY_COUNTRY = "🔍🍽 Scegli un piatto del paese *{}*."
MSG_SELECT_DISH_BY_LOCATION = "🔍🍽 Scegli un piatto della zona di Trento *{}*."
MSG_SELECT_DISH_FROM_ALL = "🔍🍽 Scegli un piatto dalla lista completa."
MSG_UPDATED_CHEF_DATA_DATA = "💨 Aggiornamento data chef completato!"
MSG_CATERING = "✍️ Scrivi le informazioni riguardo all'evento che hai bisogno di organizzare e lasciaci un breve messaggio con i recapiti per contattarti."
MSG_CATERING_RESPONSE = '🙏 Grazie per la tua richiesta. Sarai ricontattato/a il prima possibile.'
MSG_ADMIN_SCREEN = '🔑 Schermata amministratori'
MSG_ORDER_DISH = 'Ottima scelta! 🌶️\n\nContatta {} *{}* per richiedere il piatto *{}*.'


MSG_CHEF_BIO = "🏷️ *Bio*: {}"
MSG_CHEF_SPOKEN_LANGUAGES = '🌐 *Lingue Parlate*: {}'
MSG_CHEF_LOCATION = '📍 *Zona di Trento*: {}'
MSG_CHEF_AVAILABILITY = '{} è disponibile per:'
MSG_CHEF_TAKE_AWAY = '✅ *Take-Away*' 
MSG_CHEF_HOME_COOK = '✅ *Home-Cook*'

MSG_CONTACT_DICT = {
    "MAIL": lambda x: '📧 {}'.format(exmd(x.get('MAIL',''))),
    "TEL": lambda x: '☎️ {}'.format(exmd(x.get('TEL',''))),
    "TELEGRAM": lambda x: '💬 {}'.format(exmd(x.get('TELEGRAM USERNAME','')))
}

MSG_DISH_NAME = '🍽️ *{}*'
MSG_DISH_DESSCRIPTION = '📝 {}'

MSG_WRONG_INPUT_ONLY_TEXT_ACCEPTED = "⛔️ Input non valido, devi inserire solo del testo."
MSG_WRONG_INPUT_USE_TEXT = '⛔ Input non valido, per favore inserisci del testo.'
MSG_WRONG_INPUT_USE_TEXT_OR_BUTTONS = '⛔️ Input non valido, per favore inserisci del testo o usa i pulsanti 🎛'
MSG_WRONG_INPUT_INSRT_NUMBER = '⛔️🔢 Input non valido, per favore inserisci un numero.'
MSG_WRONG_INPUT_INSRT_NUMBER_BETWEEN = '⛔️🔢 Input non valido, per favore inserisci un numero da {} a {}.'
MSG_WRONG_INPUT_USE_BUTTONS = '⛔️ Input non valido, per favore usa i pulsanti 🎛'
MSG_WRONG_BUTTON_INPUT = '⛔️ Input non valido, probabilmente hai premuto un tasto due volte.'
MSG_INPUT_TOO_SHORT = '⛔️ Input troppo corto.'
MSG_INPUT_CONTAINS_SPACE_OR_MARKDOWN = '⛔️ Input non può conotenere spazi o i caratteri: "{}".'.format(utility.escape_markdown(utility.MARKDOWN_CHARS))
MSG_INPUT_NO_MARKDOWN = '⛔️ Il testo non può contenere i caratteri: {}'.format(utility.escape_markdown(utility.MARKDOWN_CHARS))
MSG_COMMAND_NOT_RECOGNIZED = '⛔️ Comando non riconosciuto.'


ALL_BUTTONS_TEXT_LIST = [v for k,v in globals().items() if k.startswith('BUTTON_')]

def text_is_button_or_digit(text):
    import utility
    return text in ALL_BUTTONS_TEXT_LIST or utility.represents_int(text)

def text_is_button(text):
    return text in ALL_BUTTONS_TEXT_LIST
