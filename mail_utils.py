from mailjet_rest import Client
import key
import json

mailjet = Client(auth=(key.MAILJET_NAME_API_KEY, key.MAILJET_SECRET_KEY), version='v3.1')

def send_message(subject, text_body, html_body=''):
    data = {
    'Messages': [
        {
        "From": {
            "Email": key.MAILJET_FROM_EMAIL,
            "Name": key.TELEGRAM_BOT_USERNAME
        },
        "To": [
            {
            "Email": key.MAILJET_TO_EMAIL,
            "Name": key.MAILJET_TO_NAME
            }
        ],
        "Subject": subject,
        "TextPart": text_body,
        "HTMLPart": html_body
        #"HTMLPart": "<h3>Dear passenger 1, welcome to <a href='https://www.mailjet.com/'>Mailjet</a>!</h3><br />May the delivery force be with you!",
        #"CustomID": "AppGettingStartedTest"
        }
    ]
    }
    result = mailjet.send.create(data=data)
    code = result.status_code
    response = result.json()
    # print(response)
    success = code==200 and response['Messages'][0]['Status']=='success'
    return success

if __name__ == "__main__":
    subject = 'mailjet test'
    text = 'this is a test from CusCusTrentoBot'
    html = ''
    success = send_message(subject, text, html)
    print(success)