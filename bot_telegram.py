# -*- coding: utf-8 -*-

import telegram
import telegram.error
import key
import logging
import traceback
import bot_ui
import time
from bot_firestore_user import User


BOT = telegram.Bot(token=key.TELEGRAM_API_TOKEN)

# see https://github.com/python-telegram-bot/python-telegram-bot/wiki/Exception-Handling
# def error_callback(bot, update, error):
#     try:
#         raise error
#     except Unauthorized:
#         # remove update.message.chat_id from conversation list
#     except BadRequest:
#         # handle malformed requests - read more below!
#     except TimedOut:
#         # handle slow connection problems
#     except NetworkError:
#         # handle other connection problems
#     except ChatMigrated as e:
#         # the chat_id of a group has changed, use e.new_chat_id instead
#     except TelegramError:
#         # handle all other telegram related errors

def exception_reporter(func):
    def exception_wrapper(*args, **kwargs):
        try:
            return func(*args, **kwargs)
        except Exception:
            report_string = '❗️ Exception {}'.format(traceback.format_exc()) #.splitlines()
            logging.error(report_string)
            try:
                report_master(report_string)
            except Exception:
                report_string = '❗️ Exception {}'.format(traceback.format_exc())
                logging.error(report_string)
    return exception_wrapper

# @exception_reporter
# def rety_on_network_error(func):
#     def retry_on_network_error_wrapper(*args, **kwargs):
#         for retry_num in range(1, 5):
#             try:
#                 return func(*args, **kwargs)
#             except telegram.error.NetworkError:
#                 sleep_secs = pow(2,retry_num)
#                 report_string = '⚠️️ Caught network error, on {} attemp. Retrying after {} secs...'.format(retry_num,sleep_secs)
#                 logging.warning(report_string)                 
#                 report_master(report_string)
#                 time.sleep(sleep_secs)
#         report_string = '❗️ Exception: persistent network error'
#         logging.error(report_string)            
#         report_master(report_string)            
#     return retry_on_network_error_wrapper

# see https://github.com/python-telegram-bot/python-telegram-bot/wiki/Exception-Handling
@exception_reporter
def telegram_error_handler(func):
    def telegram_error_wrapper(*args, **kwargs):
        user = args[0]
        try:
            return func(*args, **kwargs)            
        except telegram.error.Unauthorized as e:            
            # remove update.message.chat_id from conversation list 
            logging.debug('User has blocked Bot: {}'.format(user.chat_id))            
            user.switch_notifications()
        except telegram.error.BadRequest as e:
            # handle malformed requests - read more below!
            report_string = '⚠️️ Caught telegram BadRequest for user {}'.format(user.chat_id)
            logging.warning(report_string)                 
            report_master(report_string)
            raise(e)            
        except telegram.error.TimedOut as e:
            # handle slow connection problems
            report_string = '⚠️️ Caught telegram TimedOut for user {}'.format(user.chat_id)
            logging.warning(report_string)                 
            report_master(report_string)
            raise(e)            
        except telegram.error.NetworkError as e:
            # handle other connection problems              
            report_string = '⚠️️ Caught telegram NetworkError for user {}'.format(user.chat_id)
            logging.warning(report_string)                 
            report_master(report_string)
            raise(e)            
        except telegram.error.ChatMigrated as e:
            # the chat_id of a group has changed, use e.new_chat_id instead             
            report_string = '⚠️️ Caught telegram ChatMigrated for user {}'.format(user.chat_id)
            logging.warning(report_string)                 
            report_master(report_string)
            raise(e)            
        except telegram.error.TelegramError as e:
            # handle all other telegram related errors            
            report_string = '⚠️️ Caught telegram TelegramError for user {}'.format(user.chat_id)
            logging.warning(report_string)                 
            report_master(report_string)
            raise(e)                    
    return telegram_error_wrapper

def set_webhook():
    s = BOT.setWebhook(key.WEBHOOK_TELEGRAM_BASE, allowed_updates=['message'])
    if s:
        print("webhook setup ok: {}".format(key.WEBHOOK_TELEGRAM_BASE))
    else:
        print("webhook setup failed")

def delete_webhook():
    BOT.deleteWebhook()

def get_webhook_info():
    print(BOT.get_webhook_info())

# def send_message_query(query, text, kb=None, markdown=True, remove_keyboard=False, **kwargs):
#     def get_next_page_of_users(cursor):
#         query_iter = query.fetch(start_cursor=cursor, limit=100)
#         page = next(query_iter.pages)
#         entries = list(page)
#         next_cursor = query_iter.next_page_token
#         return entries, next_cursor
#     cursor = None
#     while(True):
#         entries, cursor = get_next_page_of_users(cursor)
#         users = [User(entry=e) for e in entries]
#         send_message_multi(users, text, kb, markdown, remove_keyboard, **kwargs)
#         #logging.debug("sending invitation to {}".format(', '.join(u.get_name() for u in users)))
#         if cursor == None:
#             break

def send_message_multi(users, text, kb=None, markdown=True, remove_keyboard=False, **kwargs):
    for u in users:
        send_message(u, text, kb=kb, markdown=markdown, remove_keyboard=remove_keyboard, sleep=True, **kwargs)


def get_reply_markup(user, kb, remove_keyboard):
    reply_markup = None
    if kb or remove_keyboard:
        if remove_keyboard:
            user.set_empy_keyboard()            
            reply_markup = telegram.ReplyKeyboardRemove()
        else:
            user.set_keyboard(kb)
            reply_markup = telegram.ReplyKeyboardMarkup(kb, resize_keyboard=True)
    return reply_markup


'''
If kb==None keep last keyboard
'''
@telegram_error_handler
def send_message(user, text, kb=None, remove_keyboard=False, markdown=True, sleep=False, **kwargs):
    #sendMessage(chat_id, text, parse_mode=None, disable_web_page_preview=None, disable_notification=False,
    # reply_to_message_id=None, reply_markup=None, timeout=None, **kwargs)
    chat_id = user.chat_id if isinstance(user, User) else user
    reply_markup = get_reply_markup(user, kb, remove_keyboard)
    parse_mode = telegram.ParseMode.MARKDOWN if markdown else None
    BOT.sendMessage(
        chat_id = chat_id,
        text = text,            
        reply_markup = reply_markup,
        parse_mode = parse_mode,
        **kwargs
    )
    if sleep:
        time.sleep(0.1)

def send_media_url(user, url_attachment, kb=None, remove_keyboard=False, caption=None, markdown=True):
    attach_type = url_attachment.split('.')[-1].lower()            
    reply_markup = get_reply_markup(user, kb, remove_keyboard)
    parse_mode = telegram.ParseMode.MARKDOWN if markdown else None
    if attach_type in ['jpg','png','jpeg']:
        BOT.send_photo(
            user.chat_id, 
            photo=url_attachment, 
            caption=caption, 
            reply_markup=reply_markup,
            parse_mode = parse_mode
        )
    elif attach_type in ['mp3']:
        BOT.send_audio(
            user.chat_id, 
            audio=url_attachment, 
            caption=caption, 
            reply_markup=reply_markup,
            parse_mode = parse_mode
        )
    elif attach_type in ['ogg']:
        BOT.send_voice(
            user.chat_id, 
            voice=url_attachment, 
            caption=caption, 
            reply_markup=reply_markup,
            parse_mode = parse_mode
        )       
    elif attach_type in ['gif']:        
        BOT.send_animation(
            user.chat_id, 
            animation=url_attachment, 
            caption=caption, 
            reply_markup=reply_markup,
            parse_mode = parse_mode
        )
    elif attach_type in ['mp4']:
        BOT.send_audio(
            user.chat_id, 
            audio=url_attachment, 
            caption=caption, 
            reply_markup=reply_markup,
            parse_mode = parse_mode
        )
    else:            
        error_msg = "Found attach_type: {}".format(attach_type)
        logging.error(error_msg)
        raise ValueError('Wrong attach type: {}'.format(error_msg))

def send_typing_action(user, sleep_secs=None):    
    chat_id = user.chat_id if isinstance(user, User) else user
    BOT.sendChatAction(
        chat_id = chat_id,
        action = telegram.ChatAction.TYPING
    )
    if sleep_secs:
        time.sleep(sleep_secs)

def send_text_document(user, file_name, file_content):
    chat_id = user.chat_id if isinstance(user, User) else user
    import requests
    files = [('document', (file_name, file_content, 'text/plain'))]
    data = {'chat_id': chat_id}
    resp = requests.post(key.TELEGRAM_API_URL + 'sendDocument', data=data, files=files)
    logging.debug("Sent documnet. Response status code: {}".format(resp.status_code))

def send_photo_from_data_multi(users, file_name, file_data, caption=None, sleep=False):    
    for u in users:
        chat_id = u.chat_id if isinstance(u, User) else u
        send_photo_from_data(chat_id, file_name, file_data, caption)        


def send_photo_from_data(user, file_name, file_data, caption=None, sleep=False):
    chat_id = user.chat_id if isinstance(user, User) else user
    import requests
    files = [('photo', (file_name, file_data, 'image/png'))]
    data = {'chat_id': chat_id}
    if caption:
        data['caption'] = caption
    resp = requests.post(key.TELEGRAM_API_URL + 'sendPhoto', data=data, files=files)
    logging.info('Sent photo. Response status code: {}'.format(resp.status_code))
    if sleep:
        time.sleep(0.1)

def get_url_from_file_id(file_id):
    import requests
    logging.debug("TELEGRAM: Requested file_id: {}".format(file_id))
    r = requests.post(key.TELEGRAM_API_URL + 'getFile', data={'file_id': file_id})
    r_json = r.json()
    if 'result' not in r_json or 'file_path' not in r_json['result']:
        logging.warning('No result found when requesting file_id: {}'.format(file_id))
        return None
    file_url = r_json['result']['file_path']
    return file_url

def get_text_from_file(file_id):
    import requests
    file_url_suffix = get_url_from_file_id(file_id)
    file_url = key.TELEGRAM_API_URL_FILE + file_url_suffix
    r = requests.get(file_url)
    return r.text

def report_master(message):
    max_length = 2000
    if len(message)>max_length:
        chunks = (message[0+i:max_length+i] for i in range(0, len(message), max_length))
        for m in chunks:
            send_message(key.TELEGRAM_BOT_MASTER_ID, m, markdown=False)    
    else:
        send_message(key.TELEGRAM_BOT_MASTER_ID, message, markdown=False)

def report_admins(message):
    max_length = 2000
    if len(message)>max_length:
        chunks = (message[0+i:max_length+i] for i in range(0, len(message), max_length))
        for m in chunks:
            send_message_multi(key.TELEGRAM_ADMIN_IDS, m, markdown=False)    
    else:
        send_message_multi(key.TELEGRAM_ADMIN_IDS, message, markdown=False)

if __name__ == "__main__":
    set_webhook()