from time import time

def flatten(L):
    ret = []
    for i in L:
        if isinstance(i,list):
            ret.extend(flatten(i))
        else:
            ret.append(i)
    return ret

def represents_int(s):
    try:
        int(s)
        return True
    except ValueError:
        return False

def represents_int_between(s, low, high):
    if not represents_int(s):
        return False
    sInt = int(s)
    if sInt>=low and sInt<=high:
        return True
    return False

MARKDOWN_CHARS = '*_`['

def contains_markdown(text):
    return any(c in text for c in MARKDOWN_CHARS)

def escape_markdown(text):
    for char in MARKDOWN_CHARS:
        text = text.replace(char, '\\'+char)
    return text

def remove_markdown(text):
    for char in MARKDOWN_CHARS:
        text = text.replace(char, '')
    return text

def add_full_stop_if_missing_end_puct(text):
    if text[-1] not in ['.','!','?']:
        text = text + '.'
    return text

def normalize_apostrophe(text):    
    for char in '’`':
        text = text.replace(char, "'")
    return text

def normalize_completion(text):
    text = add_full_stop_if_missing_end_puct(text)
    return text

def get_milliseconds():
  """
    @return Milliseconds since the epoch
  """
  return int(round(time() * 1000))

def import_url_csv_to_dict_list(url_csv, remove_new_line_escape=True): #escapeMarkdown=True
    import csv
    import requests
    r = requests.get(url_csv)
    r.encoding = "utf-8"
    spreadsheet_csv = r.text.split('\n')
    reader = csv.DictReader(spreadsheet_csv)
    if remove_new_line_escape:
        return [
            {k.replace('\\n', '\n').strip():v.replace('\\n', '\n').strip() 
            for k,v in dict.items()} for dict in reader
        ]
    return [dict for dict in reader]

def get_google_spreadsheet_dict_list(spreadsheed_id, gid):
    url = 'https://docs.google.com/spreadsheets/d/{}/export?gid={}&format=csv'.format(spreadsheed_id, gid)
    return import_url_csv_to_dict_list(url)