import logging
import bot_telegram
from bot_telegram import BOT, send_message, send_typing_action, send_text_document, \
    send_message_multi, exception_reporter, report_master, report_admins
import key
import time
import re
import bot_ui as ux
import telegram
import bot_firestore_user
from bot_firestore_user import User
import chef_data
import utility
from utility import escape_markdown as exmd
import mail_utils
import random

# ================================
# CONFIG
# ================================
DEBUG = False

# ================================
# RESTART
# ================================
def restart_multi(users):
    for u in users:
        redirect_to_state(u, state_INITIAL, message_obj=None)


def restart_user(user):
    redirect_to_state(user, state_INITIAL, message_obj=None)

# ================================
# REDIRECT TO STATE
# ================================
def redirect_to_state_multi(users, new_function, message_obj=None):
    reversed_users = list(reversed(users)) # so that game creator is last
    for u in reversed_users:
        redirect_to_state(u, new_function, message_obj)

def redirect_to_state(user, new_function, message_obj=None, set_previous_state=True):
    new_state = new_function.__name__
    if user.state != new_state:
        logging.debug("In redirect_to_state. current_state:{0}, new_state: {1}".format(str(user.state), str(new_state)))        
        user.set_state(new_state, set_previous_state)
    repeat_state(user, message_obj)

def insert_back_and_home_in_keyboard(kb):
    if len(kb)>4:
        kb.insert(0,[ux.BUTTON_BACK, ux.BUTTON_HOME]) 
    kb.append([ux.BUTTON_BACK, ux.BUTTON_HOME]) 

# ================================
# REPEAT STATE
# ================================
def repeat_state(user, message_obj=None):
    state = user.state
    if state is None:
        restart_user(user)
        return
    method = possibles.get(state)
    if not method:
        msg = "⚠️ User {} sent to unknown method state: {}".format(user.chat_id, state)
        report_master(msg)
        send_message(user, ux.MSG_REFRESH_INTERFACE, sleep=1)
        restart_user(user)
    else:
        method(user, message_obj)

# ================================
# Initial State
# ================================
def state_INITIAL(user, message_obj):
    if message_obj is None:
        kb = [
            [ux.BUTTON_SEARCH_DISH], #, ux.BUTTON_SEARCH_CHEF
            [ux.BUTTON_CATERING],
            [ux.BUTTON_INFO]
        ]
        if user.is_admin():
            kb[2].insert(1, ux.BUTTON_ADMIN)
        # notifications_button = [ux.BUTTON_DISABLE_NOTIFICATIONS] if user.notifications else [ux.BUTTON_ENABLE_NOTIFICATIONS]
        # kb.append(notifications_button)
        # msg_notifications = ux.MSG_NOTIFICATIONS_ON if user.notifications else ux.MSG_NOTIFICATIONS_OFF        
        chef_info = chef_data.get_chef_data_by_id(user.chat_id)
        if chef_info:            
            male = chef_info['SESSO'].upper()=='UOMO'
            msg = ux.MSG_HOME_CHEF_M if male else ux.MSG_HOME_CHEF_F
            send_message(user, msg, kb)
        else:
            msg = ux.MSG_HOME_TEST if key.TEST else ux.MSG_HOME
            send_message(user, msg, kb)
    else:
        text_input = message_obj.text
        kb = user.get_keyboard()
        if text_input:
            if text_input in utility.flatten(kb):
                if text_input == ux.BUTTON_INFO:
                    send_message(user, ux.MSG_INFO)
                elif text_input == ux.BUTTON_SEARCH_DISH:
                    redirect_to_state(user, state_DISH_SEARCH_CRITERION)
                # elif text_input == ux.BUTTON_SEARCH_CHEF:
                #     pass
                elif text_input == ux.BUTTON_CATERING:
                    redirect_to_state(user, state_CATERING)
                elif text_input == ux.BUTTON_ADMIN:
                    redirect_to_state(user, state_ADMIN)
                else:
                    send_message(user, ux.MSG_REFRESH_INTERFACE)
                    send_typing_action(user, sleep_secs=1)
                    repeat_state(user)
            elif ux.text_is_button(text_input):
                send_message(user, ux.MSG_WRONG_BUTTON_INPUT, kb)
            else:
                send_message(user, ux.MSG_WRONG_INPUT_USE_BUTTONS, kb)
        else:
            send_message(user, ux.MSG_WRONG_INPUT_USE_BUTTONS, kb)

# ================================
# Search Dish by Criterion
# ================================
def state_DISH_SEARCH_CRITERION(user, message_obj):
    if message_obj is None:
        kb = [            
            [ux.BUTTON_SEARCH_BY_COUNTRY, ux.BUTTON_SEARCH_BY_LOCATION],
            [ux.BUTTON_SEARCH_ALL],
            [ux.BUTTON_HOME]
        ]        
        send_message(user, ux.MSG_SELECT_DISH_SEARCH_CRITERION, kb)
    else:
        text_input = message_obj.text
        kb = user.get_keyboard()
        if text_input:
            if text_input in utility.flatten(kb):
                if text_input == ux.BUTTON_HOME:
                    restart_user(user)
                else:
                    user.set_var('DISH_SEARCH_CRITERION', text_input)
                    if text_input == ux.BUTTON_SEARCH_ALL:
                        redirect_to_state(user, state_DISH_SELECTION)
                    else:
                        redirect_to_state(user, state_DISH_TYPE)
            elif ux.text_is_button(text_input):
                send_message(user, ux.MSG_WRONG_BUTTON_INPUT, kb)
            else:
                send_message(user, ux.MSG_WRONG_INPUT_USE_BUTTONS, kb)
        else:
            send_message(user, ux.MSG_WRONG_INPUT_USE_BUTTONS, kb)

# ================================
# Select values within selected criterion
# ================================
def state_DISH_TYPE(user, message_obj):
    search_criterion = user.get_var('DISH_SEARCH_CRITERION')
    if message_obj is None:        
        if search_criterion == ux.BUTTON_SEARCH_BY_COUNTRY:
            criterion_list = sorted(chef_data.CHEF_COUNTRIES)
            criterion_list = [ux.insert_flag(c) for c in criterion_list]
            msg = ux.MSG_SELECT_COUNTRY
        elif search_criterion == ux.BUTTON_SEARCH_BY_LOCATION:
            criterion_list = sorted(chef_data.CHEF_LOCATIONS)
            msg = ux.MSG_SELECT_LOCATION
        else:
            assert(False)        
        kb = [[x] for x in criterion_list]
        insert_back_and_home_in_keyboard(kb)
        send_message(user, msg, kb)
    else:
        text_input = message_obj.text
        kb = user.get_keyboard()
        if text_input:
            if text_input in utility.flatten(kb):
                if text_input == ux.BUTTON_HOME:
                    restart_user(user)
                elif text_input == ux.BUTTON_BACK:
                    previous_state = possibles[user.get_previous_state()]
                    redirect_to_state(user, previous_state, set_previous_state=False)
                else:
                    if search_criterion == ux.BUTTON_SEARCH_BY_COUNTRY:
                        text_input = text_input.split(' ',1)[1] # remove flag
                    user.set_var('SELECTED_DISH_TYPE', text_input)
                    redirect_to_state(user, state_DISH_SELECTION)
            elif ux.text_is_button(text_input):
                send_message(user, ux.MSG_WRONG_BUTTON_INPUT, kb)        
            else:
                send_message(user, ux.MSG_WRONG_INPUT_USE_BUTTONS, kb)
        else:
            send_message(user, ux.MSG_WRONG_INPUT_USE_BUTTONS, kb)

# ================================
# Chef Selection
# ================================
'''
def state_CHEF_SELECTION(user, message_obj):
    if message_obj is None:
        search_criterion = user.get_var('DISH_SEARCH_CRITERION')
        if search_criterion==ux.BUTTON_SEARCH_ALL:
            chef_selected_data = chef_data.CHEF_DATA
        else:
            search_criterion_value =  user.get_var('SELECTED_DISH_TYPE')
            if search_criterion == ux.BUTTON_SEARCH_BY_COUNTRY:
                chef_selected_data = chef_data.get_chef_data_by_country(search_criterion_value)
            elif search_criterion == ux.BUTTON_SEARCH_BY_LOCATION:
                chef_selected_data = chef_data.get_chef_data_by_location(search_criterion_value)
            else:
                assert(False)
        chef_nomi_cognomi_list = ['{} {}'.format(d['NOME'],d['COGNOME']) for d in chef_selected_data]
        random.shuffle(chef_nomi_cognomi_list)
        kb = [[c] for c in  chef_nomi_cognomi_list]
        kb.append([ux.BUTTON_HOME])
        send_message(user, ux.MSG_SELECT_CHEF, kb)
    else:
        text_input = message_obj.text
        kb = user.get_keyboard()
        if text_input:
            if text_input in utility.flatten(kb):
                if text_input == ux.BUTTON_HOME:
                    restart_user(user)
                else:
                    chef_chat_id = next(
                        d['TELEGRAM ID'] for d in chef_data.CHEF_DATA \
                        if text_input=='{} {}'.format(d['NOME'],d['COGNOME'])
                    )
                    user.set_var('SELECTED_CHEF_CHAT_ID', chef_chat_id)
                    redirect_to_state(user, state_CHEF_INFO)
            elif ux.text_is_button(text_input):
                send_message(user, ux.MSG_WRONG_BUTTON_INPUT, kb)                
            else:
                send_message(user, ux.MSG_WRONG_INPUT_USE_BUTTONS, kb)
        else:                
            send_message(user, ux.MSG_WRONG_INPUT_USE_BUTTONS, kb)
'''

# ================================
# Select dish in list
# ================================
def state_DISH_SELECTION(user, message_obj):
    if message_obj is None:
        search_criterion = user.get_var('DISH_SEARCH_CRITERION')
        if search_criterion==ux.BUTTON_SEARCH_ALL:
            dish_data = chef_data.DISH_DATA
            msg =ux.MSG_SELECT_DISH_FROM_ALL
        else:
            search_criterion_value =  user.get_var('SELECTED_DISH_TYPE')
            if search_criterion == ux.BUTTON_SEARCH_BY_COUNTRY:                
                dish_data = chef_data.get_dish_data_of_country(search_criterion_value)
                flag_country = ux.insert_flag(search_criterion_value)
                msg = ux.MSG_SELECT_DISH_BY_COUNTRY.format(flag_country)
            elif search_criterion == ux.BUTTON_SEARCH_BY_LOCATION:
                dish_data = chef_data.get_dish_name_list_by_location(search_criterion_value)
                msg = ux.MSG_SELECT_DISH_BY_LOCATION.format(search_criterion_value)
            else:
                assert(False)        
        dish_data = sorted(dish_data, key=lambda x: x['NOME ORIGINALE'])
        dish_country_names = [ux.insert_flag(d['PAESE'], d['NOME ORIGINALE']) for d in  dish_data]
        kb = [[d] for d in dish_country_names]
        insert_back_and_home_in_keyboard(kb)
        send_message(user, msg, kb)
    else:
        text_input = message_obj.text
        kb = user.get_keyboard()
        if text_input:
            if text_input in utility.flatten(kb):
                if text_input == ux.BUTTON_HOME:
                    restart_user(user)
                elif text_input == ux.BUTTON_BACK:
                    previous_state = possibles[user.get_previous_state()]
                    redirect_to_state(user, previous_state, set_previous_state=False)
                else:
                    text_input = text_input.split(' ',1)[1] # remove flag
                    user.set_var('SELECTED_DISH', text_input)
                    redirect_to_state(user, state_DISH_DETAILS)
            elif ux.text_is_button(text_input):
                send_message(user, ux.MSG_WRONG_BUTTON_INPUT, kb)
            else:
                send_message(user, ux.MSG_WRONG_INPUT_USE_BUTTONS, kb)                
        else:
            send_message(user, ux.MSG_WRONG_INPUT_USE_BUTTONS, kb)

# ================================
# Dish Info
# ================================
def state_DISH_DETAILS(user, message_obj):
    if message_obj is None:
        dish_name = user.get_var('SELECTED_DISH')        
        chef_info, dish_info = chef_data.get_chef_dish_info_from_dish_name(dish_name)
        user.set_var('SELECTED_CHEF_CHAT_ID', chef_info['TELEGRAM ID'])        
        dish_country = chef_info.get('PAESE','')
        chef_is_man = chef_info['SESSO'].upper() == 'UOMO'
        chef_icon = ux.CHEF_M if chef_is_man else ux.CHEF_F
        photo_url = dish_info.get('FOTO', None)
        descrizione_no_markdown = utility.remove_markdown(dish_info.get('DESCRIZIONE', ''))
        msg_list = [
            ux.MSG_DISH_NAME.format(dish_name),
            '{} {} {}'.format(chef_icon, chef_info['NOME'], chef_info['COGNOME']),                        
            ux.insert_flag(dish_country),
            ux.MSG_DISH_DESSCRIPTION.format(descrizione_no_markdown) 
        ]
        msg = '\n'.join(msg_list)
        chef_info_button = ux.BUTTON_CHEF_M_INFO if chef_is_man else ux.BUTTON_CHEF_F_INFO
        kb = [[chef_info_button],[ux.BUTTON_ORDER_DISH], [ux.BUTTON_BACK, ux.BUTTON_HOME]]     
        if photo_url:
            bot_telegram.send_media_url(user, photo_url, kb=kb, caption=msg)   
        else:
            send_message(user, msg, kb)
    else:
        text_input = message_obj.text
        kb = user.get_keyboard()
        if text_input:
            if text_input in utility.flatten(kb):
                if text_input == ux.BUTTON_HOME:
                    restart_user(user)
                elif text_input == ux.BUTTON_BACK:
                    previous_state = possibles[user.get_previous_state()]
                    redirect_to_state(user, previous_state, set_previous_state=False)
                elif text_input in [ux.BUTTON_CHEF_M_INFO, ux.BUTTON_CHEF_F_INFO]:                    
                    redirect_to_state(user, state_CHEF_INFO)
                elif text_input == ux.BUTTON_ORDER_DISH:
                    redirect_to_state(user, state_ORDER_DISH)
                    # send_message(user, ux.MSG_TO_BE_IMPLEMENTED, kb)
                else:
                    assert(False)
            elif ux.text_is_button(text_input):
                send_message(user, ux.MSG_WRONG_BUTTON_INPUT, kb)
            else:
                send_message(user, ux.MSG_WRONG_INPUT_USE_BUTTONS, kb)
        else:
            send_message(user, ux.MSG_WRONG_INPUT_USE_BUTTONS, kb)

# ================================
# Chef Info
# ================================
def state_CHEF_INFO(user, message_obj):
    if message_obj is None:
        chef_chat_id = user.get_var('SELECTED_CHEF_CHAT_ID')        
        chef_info = chef_data.get_chef_data_by_id(chef_chat_id)
        chef_icon = ux.CHEF_M if chef_info['SESSO'].upper() == 'UOMO' else ux.CHEF_F    
        take_away = chef_info.get('ESPERIENZA TAKE-AWAY',False)
        home_cook = chef_info.get('ESPERIENZA HOME-COOK',False)
        photo_url = chef_info.get('FOTO', None)
        msg_chef_info_list = [        
            '{} *{} {}*'.format(chef_icon, chef_info['NOME'], chef_info['COGNOME']),            
            '',            
            ux.insert_flag(chef_info['PAESE']),
            ux.MSG_CHEF_SPOKEN_LANGUAGES.format(chef_info['LINGUE PARLATE']),        
            ux.MSG_CHEF_LOCATION.format(chef_info['LUOGO']),            
        ]
        if take_away or home_cook:
            msg_chef_info_list.extend([
                '',
                ux.MSG_CHEF_AVAILABILITY.format(chef_info['NOME'])
            ])
            if take_away:
                msg_chef_info_list.append(ux.MSG_CHEF_TAKE_AWAY)
            if home_cook:
                msg_chef_info_list.append(ux.MSG_CHEF_HOME_COOK)

        bio_no_markdown = utility.remove_markdown(chef_info['BREVE DESCRIZIONE PERSONALE'])
        if bio_no_markdown:
            msg_chef_info_list.extend([
                '',
                ux.MSG_CHEF_BIO.format(bio_no_markdown),            
            ])

        msg_chef_info = '\n'.join(msg_chef_info_list)
        kb = [[ux.BUTTON_BACK, ux.BUTTON_HOME]]
        if photo_url:
            bot_telegram.send_media_url(user, photo_url, kb=kb, caption=msg_chef_info)   
        else:
            send_message(user, msg_chef_info, kb)        
    else:
        text_input = message_obj.text
        kb = user.get_keyboard()
        if text_input:
            if text_input in utility.flatten(kb):                
                if text_input == ux.BUTTON_HOME:
                    restart_user(user)
                elif text_input == ux.BUTTON_BACK:
                    previous_state = possibles[user.get_previous_state()]
                    redirect_to_state(user, previous_state, set_previous_state=False)
                else:
                    user.set_var('SELECTED_DISH', text_input)
                    redirect_to_state(user, state_DISH_DETAILS)
            elif ux.text_is_button(text_input):
                send_message(user, ux.MSG_WRONG_BUTTON_INPUT, kb)             
            else:
                send_message(user, ux.MSG_WRONG_INPUT_USE_BUTTONS, kb)
        else:                
            send_message(user, ux.MSG_WRONG_INPUT_USE_BUTTONS, kb)

# ================================
# Chef Order Dish
# ================================
def state_ORDER_DISH(user, message_obj):
    if message_obj is None:
        dish_name = user.get_var('SELECTED_DISH')        
        chef_chat_id = user.get_var('SELECTED_CHEF_CHAT_ID')        
        chef_info = chef_data.get_chef_data_by_id(chef_chat_id)
        chef_icon = ux.CHEF_M if chef_info['SESSO'].upper() == 'UOMO' else ux.CHEF_F    
        contact_preferences = chef_info.get("MODALITA' DI CONTATTO PREFERITA (tel o mail)","")
        contact_preferences = [x.strip().upper() for x in contact_preferences.split(',')]
        photo_url = chef_info.get('FOTO', None)  
        msg_contact_info_list = [
            ux.MSG_ORDER_DISH.format(chef_icon, chef_info['NOME'], dish_name)
        ]      
        msg_contact_info_list.extend([
            ux.MSG_CONTACT_DICT[c](chef_info) 
            for c in contact_preferences 
            if c in ux.MSG_CONTACT_DICT
        ])
        msg_contact_info = '\n'.join(msg_contact_info_list)
        kb = [[ux.BUTTON_BACK, ux.BUTTON_HOME]]
        # if photo_url:
        #     bot_telegram.send_media_url(user, photo_url, kb=kb, caption=msg_contact_info)   
        # else:
        send_message(user, msg_contact_info, kb)        
    else:
        text_input = message_obj.text
        kb = user.get_keyboard()
        if text_input:
            if text_input in utility.flatten(kb):                
                if text_input == ux.BUTTON_HOME:
                    restart_user(user)
                elif text_input == ux.BUTTON_BACK:
                    previous_state = possibles[user.get_previous_state()]
                    redirect_to_state(user, previous_state, set_previous_state=False)
            elif ux.text_is_button(text_input):
                send_message(user, ux.MSG_WRONG_BUTTON_INPUT, kb)             
            else:
                send_message(user, ux.MSG_WRONG_INPUT_USE_BUTTONS, kb)
        else:                
            send_message(user, ux.MSG_WRONG_INPUT_USE_BUTTONS, kb)

# ================================
# Catering Request
# ================================
def state_CATERING(user, message_obj):
    if message_obj is None:
        kb = [
            [ux.BUTTON_BACK]
        ]
        send_message(user, ux.MSG_CATERING, kb)
    else:
        text_input = message_obj.text
        kb = user.get_keyboard()
        if text_input:
            if text_input in utility.flatten(kb):
                if text_input == ux.BUTTON_BACK:
                    restart_user(user)
                else:
                    assert(False)
            elif ux.text_is_button(text_input):
                send_message(user, ux.MSG_WRONG_BUTTON_INPUT, kb)
            else:                
                subject = '[CusCusTrentoBot] Richiesta di evento'
                text_body = (
                    'Nuova richiesta di evento\n\n'
                    'Utente: {} ({})\n'
                    'Messaggio: {}'
                ).format(user.get_name_at_username(), user.chat_id, text_input)
                success = mail_utils.send_message(subject, text_body)
                if not success:
                    error_msg = "❗ Problem in sending richiesta evento email:\n{}".format(text_body)
                    report_master(error_msg)
                redirect_to_state(user, state_CATERING_RESPONSE)
        else:
            send_message(user, ux.MSG_WRONG_INPUT_USE_TEXT_OR_BUTTONS, kb)

# ================================
# Catering Response
# ================================
def state_CATERING_RESPONSE(user, message_obj):
    if message_obj is None:
        kb = [
            [ux.BUTTON_HOME]
        ]
        send_message(user, ux.MSG_CATERING_RESPONSE, kb)
    else:
        text_input = message_obj.text
        kb = user.get_keyboard()
        if text_input:
            if text_input in utility.flatten(kb):
                if text_input == ux.BUTTON_HOME:
                    restart_user(user)
                else:
                    assert(False)
            elif ux.text_is_button(text_input):
                send_message(user, ux.MSG_WRONG_BUTTON_INPUT, kb)         
            else:
                send_message(user, ux.MSG_WRONG_INPUT_USE_BUTTONS, kb)
        else:                
            send_message(user, ux.MSG_WRONG_INPUT_USE_BUTTONS, kb)

# ================================
# Catering Response
# ================================
def state_ADMIN(user, message_obj):
    if message_obj is None:
        kb = [
            [ux.BUTTON_STATS],
            [ux.BUTTON_REFRESH_CHEF_DATA],
            [ux.BUTTON_HOME]
        ]
        send_message(user, ux.MSG_ADMIN_SCREEN, kb)
    else:
        text_input = message_obj.text
        kb = user.get_keyboard()
        if text_input:
            if text_input in utility.flatten(kb):
                if text_input == ux.BUTTON_STATS:
                    user_num = User.get_user_count()
                    chef_num, chef_names = chef_data.get_chef_number_and_names()
                    msg = (
                        '*Numero utenti*: {}\n'
                        '*Chef* ({}): {}'
                    ).format(user_num, chef_num, chef_names)
                    send_message(user, msg)
                elif text_input == ux.BUTTON_REFRESH_CHEF_DATA:
                    chef_data.update_data()
                    send_message(user, ux.MSG_UPDATED_CHEF_DATA_DATA)
                elif text_input == ux.BUTTON_HOME:
                    restart_user(user)
                else:
                    assert(False)
            elif ux.text_is_button(text_input):
                send_message(user, ux.MSG_WRONG_BUTTON_INPUT, kb)
            else:
                send_message(user, ux.MSG_WRONG_INPUT_USE_BUTTONS, kb)
        else:                
            send_message(user, ux.MSG_WRONG_INPUT_USE_BUTTONS, kb)


def deal_with_universal_commands(user, text_input):
    #logging.debug('In universal command with input "{}". User is master: {}'.format(text_input, user.is_master()))
    if text_input == '/start':
        send_message(user, ux.MSG_WELCOME)
        restart_user(user)
        return True
    if text_input == '/state':
        s = user.state
        msg = "You are in state {}".format(s)
        send_message(user, msg, markdown=False)
        return True
    if text_input == '/refresh':
        repeat_state(user)
        return True

    if user.is_master():
        if text_input == '/update':
            chef_data.update_data()
            send_message(user, ux.MSG_UPDATED_CHEF_DATA_DATA)
            return True
        if text_input == '/debug':
            import json
            send_text_document(user, 'tmp_vars.json', json.dumps(user.variables))
            return True
        if text_input.startswith('/debug '):
            import json
            chat_id = text_input.split()[1]
            debug_user = User.get_user('telegram', chat_id)
            send_text_document(user, 'tmp_vars.json', json.dumps(debug_user.variables))
            return True
        if text_input == '/exception':
            1/0
            return True
    return False

# ================================
# DEAL WITH REQUEST
# ================================
'''
python-telegram-bot documentation
https://python-telegram-bot.readthedocs.io/en/stable/
'''
@exception_reporter
def deal_with_request(request_json):
    # retrieve the message in JSON and then transform it to Telegram object
    update_obj = telegram.Update.de_json(request_json, BOT)
    message_obj = update_obj.message
    user_obj = message_obj.from_user
    username = user_obj.username
    last_name = user_obj.last_name if user_obj.last_name else ''
    name = (user_obj.first_name + ' ' + last_name).strip()
    language = user_obj.language_code
    
    user = User.get_user('telegram', user_obj.id)
    if user == None:
        user = User.create_user('telegram', user_obj.id, name, username, language)
        report_admins('New user: {}'.format(user.get_name_at_username()))
    else:
        user.update_user(name, username)

    if message_obj.text:
        text_input = message_obj.text        
        logging.debug('Message from @{} in state {} with text {}'.format(user.chat_id, user.state, text_input))
        if DEBUG and not user.is_admin():
            send_message(user, ux.MSG_WORK_IN_PROGRESS)
            return
        if deal_with_universal_commands(user, text_input):
            return
        repeat_state(user, message_obj=message_obj)
    else:
        send_message(user, ux.MSG_WRONG_INPUT_ONLY_TEXT_ACCEPTED)

possibles = globals().copy()
possibles.update(locals())

