import key
import requests
import utility

# headers CUOCHI:
'''
ID	
TELEGRAM ID
TELEGRAM USERNAME
NOME	
COGNOME	
PAESE	
FLAG
SESSO	
LUOGO	
LINGUE PARLATE	
RICETTE ID	
BREVE DESCRIZIONE PERSONALE	
CUSCUS CHEF	
FB LINK	
ID ATTESTATI	
ID EVENTI PORTFOLIO	
ESPERIENZA TAKE-AWAY	
ESPERIENZA HOME-COOK 	
MODALITA' DI SCAMBIO PREFERITA (denaro o baratto)	
TEL	
MAIL	
MODALITA' DI CONTATTO PREFERITA (tel o mail)
'''

# headers ricette
'''
ID
NOME ORIGINALE	
NOME COMMERCIALE	
DESCRIZIONE	PICCANTE	
VEGETARIANO	VEGANO	
HALAL	
SENZA GLUTINE	
SENZA LATTOSIO	
TAKE-AWAY	
HOME-COOK	
FOTO
'''

# headers attestati
'''
ID
Nome Attestato
Data di rilascio
Ore Formazione
Valido a Norma di legge
'''

CHEF_DATA = None
CHEF_IDS = None
CHEF_COUNTRIES = None
CHEF_LOCATIONS = None
DISH_DATA = None
# headers as key in dict with in addition:
'''
RICETTE
ATTESTATI
EVENTI PORTFOLIO
'''

def get_cuochi_data():
    ATTESTATI_DATA = utility.get_google_spreadsheet_dict_list(key.GDOC_CUOCHI_ID, key.ATTESTATI_GID)
    EVENTI_PORTFOLIO_DATA = utility.get_google_spreadsheet_dict_list(key.GDOC_CUOCHI_ID, key.EVENTI_PORTFOLIO_GID)
    dict_list = utility.get_google_spreadsheet_dict_list(key.GDOC_CUOCHI_ID, 0)
    dict_list = [d for d in dict_list if d['TELEGRAM ID']]    
    for c in dict_list:
        # RICETTE
        ricette_gid = c.get('RICETTE GID',None)        
        ricette_list = []
        if ricette_gid:
            ricette_list = utility.get_google_spreadsheet_dict_list(key.GDOC_CUOCHI_ID,ricette_gid)
            ricette_list = [d for d in ricette_list if d.get('NOME ORIGINALE',None)]        
            for r in ricette_list:
                r['PAESE'] = c.get('PAESE')
                r['LUOGO'] = c.get('LUOGO')
        c['RICETTE_GID'] = ricette_gid
        c['RICETTE'] = ricette_list                
        # ATTESTATI
        attestati_list = []
        attestati_ids = c.get('ID ATTESTATI',None)
        if attestati_ids:
            attestati_ids_list = [i.strip() for i in attestati_ids.split(',')]
            attestati_list = [a for a in ATTESTATI_DATA if a['ID'] in attestati_ids_list]
        c['ATTESTATI'] = attestati_list
        # EVENTI POTFOLIO
        eventi_portfolio_list = []
        eventi_portfolio_ids = c.get('ID EVENTI PORTFOLIO',None)
        if eventi_portfolio_ids:
            eventi_portfolio_list = [i.strip() for i in eventi_portfolio_ids.split(',')]
            eventi_portfolio_list = [e for e in EVENTI_PORTFOLIO_DATA if e['ID'] in eventi_portfolio_list]
        c['EVENTI PORTFOLIO'] = eventi_portfolio_list
    return dict_list

def update_data():
    global CHEF_DATA, CHEF_IDS, CHEF_COUNTRIES, CHEF_LOCATIONS, DISH_DATA
    CHEF_DATA = get_cuochi_data()
    CHEF_IDS = [d['TELEGRAM ID'] for d in CHEF_DATA]
    CHEF_COUNTRIES = list(set([d['PAESE'] for d in CHEF_DATA]))
    CHEF_LOCATIONS = list(set([d['LUOGO'] for d in CHEF_DATA]))
    DISH_DATA = [c for d in CHEF_DATA for c in d['RICETTE']]

def get_dish_name_full_list():
    return [d['NOME ORIGINALE'] for d in DISH_DATA]

def get_dish_data_of_country(country):
    return [d for d in DISH_DATA if d.get('PAESE','') == country]

def get_dish_name_list_by_location(location):
    return [d for d in DISH_DATA if d.get('LUOGO','') == location]

def get_dish_name_of_chef(chef_chat_id):
    chef_info = next((c for c in CHEF_DATA if c['TELEGRAM ID']==chef_chat_id),None)
    if chef_info:
        chef_dishes = chef_info['RICETTE']
        return [d['NOME ORIGINALE'] for d in chef_dishes]
    return None

def get_chef_dish_info_from_dish_name(dish_name):
    for chef_info in CHEF_DATA:
        chef_dishes = chef_info['RICETTE']
        for dish_info in chef_dishes:
            if dish_info['NOME ORIGINALE']==dish_name:                                
                return chef_info, dish_info
    return None, None

def get_chef_data_from_chat_id(chat_id):
    import bot_ui as ux
    return next((d for d in CHEF_DATA if d['TELEGRAM ID']==chat_id),None)    



def get_chef_data_by_id(chat_id):
    if chat_id in CHEF_IDS:
        return next(d for d in CHEF_DATA if d['TELEGRAM ID']==chat_id)
    return None

def get_chef_data_by_country(country):
    return [d for d in CHEF_DATA if d['PAESE']==country]

def get_chef_data_by_location(loc):
    return [d for d in CHEF_DATA if d['LUOGO']==loc]

def get_chef_number_and_names():
    num = len(CHEF_DATA)
    names = ', '.join(['{} {}'.format(x['NOME'], x['COGNOME']).strip() for x in CHEF_DATA])
    return num, names

update_data()